# SLight

Mediawiki extension.

## Description

* Version 1.0
* Extension allows inserting Silverlight content.
* Silverlight app (XAP extension) can be placed this way:

```xml
<slight width="width px" height="height px">wikifilename (with no "File:")</slight>
```

* Width is optional, default is 100%. 
* Percentage of height doesn't work in IE for example, so it must be set in pixels.

## Instalation

* Make sure you have MediaWiki 1.29+ installed.
* Download and place the extension to your /extensions/ folder.
* Add the following code to your LocalSettings.php: `wfLoadExtension( 'SLight' )`;
* XAP (it's zip actually) file must be uploaded into the wiki due to security permissions.
* Uploading XAPs we have to allow:
	* LocalSettings.php: `$wgFileExtensions[] = 'zip'; $wgFileExtensions[] = 'xap';`
    * Includes/mime.types
	    * add "xap" at the end of row with "application/zip"
		* at the end of the file add following rows:
```
	application/xaml+xml xaml
	application/x-silverlight-app xap
```

## Authors and license

* [Josef Martiňák](https://www.wikiskripta.eu/w/User:Josmart)
* MIT License, Copyright (c) 2018 First Faculty of Medicine, Charles University
