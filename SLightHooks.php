<?php

/**
 * All hooked functions used by SLight
 * @ingroup Extensions
 * @author Josef Martiňák
 * @license MIT
 * @file
 */

class SLightHooks {

	/**
	 * Set up the parser hooks
	 * @param object $parser: instance of Parser object
	 * @return Boolean: true
	 */
	public static function registerParserHook(&$parser) {
		$parser->setHook('slight', 'SLightHooks::slightRender');
		return true;
	}
	
	
	/**
	 * Callback function for registerParserHook
	 * @param $input String: user-supplied input, URL of the Silverlight
	 * @param $args Array: user-supplied arguments, width, height
	 * @param $parser Parser: instance of Parser
	 * @return String: HTML
	 */
	public static function slightRender($input,$args,$parser) {
	
		// set atributes
		if(isset($args["width"])) {
			$width = $args["width"];
		}
		else {
			$width = "100%";
		}
	
		if(isset($args["height"])) {
			$height = $args["height"];
		}
		else {
			return "<div style='color:red;'>Height of the Silverlight application is not set !!!</div>";
		}
	
		// input can be either URL or wikifile
		if(isset($input)) {
			$title = Title::newFromText($input,6);
			if($title->exists()){
				$input = wfFindFile($title)->getFullURL();
				$output = "<div><object id='SilverlightPlugin1' width='$width' height='$height' ";
				$output .= "data='data:application/x-silverlight-2,' "; 
				$output .= "type='application/x-silverlight-2'>\n";
				$output .= "<param name='source' value='$input'/>\n";
				// Display installation image, if SL not installed
				$output .= "<a href='http://go.microsoft.com/fwlink/?LinkID=149156&v=4.0.60310.0' ";
				$output .= "style='text-decoration: none;'>";
				$output .= "<img src='http://go.microsoft.com/fwlink/?LinkId=161376' ";
				$output .= "alt='Get Microsoft Silverlight' style='border-style: none'/></a>\n";
				$output .= "</object>\n";
				// for cross-browser compatibility - Safari issue
				$output .= "<iframe id='_sl_historyFrame' style='visibility:hidden;";
				$output .= "height:0px;width:0px;border:0px'></iframe>\n</div>\n";
				return $output;
			}
			else{
				return "<div style='color:red;'>There is no file \"$input\" on this wiki !!!</div>";
			}
		}
	}
	
}